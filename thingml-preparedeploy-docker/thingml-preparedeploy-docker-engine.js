//This function call ThingML for compilation
var fs = require('fs');
var path = require('path');
var events = require('events');
var handlebars = require('handlebars');

function copyDirContentRecursive(src, dst, callback) {
    if (fs.existsSync(src)) {
        var files = fs.readdirSync(src);

        files.forEach(function(file) {
            var srcFile = path.resolve(src, file+'');
            var destFile = path.resolve(dst, file+'');
            stat = fs.lstatSync(srcFile);

            if (stat.isDirectory()) {
                fs.mkdirSync(destFile);
                copyDirContent(srcFile, destFile);
            } else {
                fs.copyFileSync(srcFile, destFile);
            }
        });
    }
    callback();
}

function fillTemplate(src, dst, data, callback) {
    try {
        fs.readFile(src, 'utf-8', function(error, source) {
            if (error) {
                return callback(error);
            }
            try {
                // Fill template
                var template = handlebars.compile(source);
                var dockercompose = template(data);

                // Write generated file
                fs.writeFile(dst, dockercompose, function (error) {
                    if (error) {
                        return callback(error);
                    }
                    callback();
                });
            } catch (e) {
                callback(e);
            }
        });
    } catch(e) {
        callback(e);
    }
}

module.exports = class ThingMLPreparedeployDockerEngine {
    constructor() {
        this.emitter = new events.EventEmitter();
    }


    preparedeploy(node, config, msg) {
        var engine = this;

        // source template dir (docker-compose.yml.hbs and Dockerfile.hbs
        var sourceTemplateDir = __dirname + '/data/templates/';

        // destination for filled template (docker-compose.yml and Dockerfile.hbs)
        config.path = 'generated/' + msg.name + '/';

        // default configuration data to fill in the template
        var defaultData = {
            name: config.servicename || msg.name,
            image: config.imagename || 'node-red-thingml-' + msg.name.toLowerCase(),
            commands: [
                'ip -4 route list match 0/0 | awk \'{print $$3" host.docker.internal"}\' >> /etc/hosts ', // https://github.com/docker/for-linux/issues/264
            ],
            ports: config.portsmapping,
            workingdir: config.workingdir,
            dockerfile: config.dockerfile || 'Dockerfile'
        };

        // target specific configuration data to fill in the template
        var targetData = require('./targets/' + msg.target + '/preparedeploy')(msg);

        // append commands volumes and ports from target specific configuration data to default configuration data
        if (targetData.commands) {
            targetData.commands = defaultData.commands.concat(targetData.commands);
        }
        if (targetData.volumes) {
            targetData.volumes = defaultData.volumes.concat(targetData.volumes);
        }
        if (targetData.ports) {
            targetData.ports = defaultData.ports.concat(targetData.ports);
        }
        // merge default data and target specific configuration data
        var data = Object.assign(defaultData, targetData);

        // fill and write generated docker-compose.yml
        var dockercomposeout = config.path + 'docker-compose.yml';
        fillTemplate(sourceTemplateDir + 'docker-compose.yml.hbs', dockercomposeout, data, function(error) {
            if (error) {
                return engine.emitter.emit('error', error);
            }
            node.log("The file was saved as " + dockercomposeout);

            // Copy target-specific data files to destination
            copyDirContentRecursive(__dirname + '/targets/' + msg.target + '/dest', 'generated/' + msg.name, function(err) {
                if (error) {
                    return engine.emitter.emit('error', error);
                }
                engine.emitter.emit('prepared');
            });
        });
    }

    close(node) {
        this.emitter.removeAllListeners();
    };

    on(e, f) {
        this.emitter.on(e, f);
    }
}
