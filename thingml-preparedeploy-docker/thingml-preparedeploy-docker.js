module.exports = function(RED) {
    "use strict";
    var ThingMLPreparedeployDockerEngine = require("./thingml-preparedeploy-docker-engine");

    function ThingMLPrepareDeployDockerNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        var engine = new ThingMLPreparedeployDockerEngine();

        // Received message, trigger build
        node.on('input', function(msg) {
            node.status({fill:"yellow",shape:"dot",text:"thingml-preparedeploy-docker.status.preparing"});
            engine.preparedeploy(node, config, msg.payload);
        });

        engine.on('prepared', function() {
            node.status({fill:"green",shape:"dot",text:"thingml-preparedeploy-docker.status.success"});
            var msg = { payload: config };
            node.send(msg);
        });

        engine.on('error', function(e) {
            node.status({fill:"red",shape:"ring",text:"node-red:common.status.error"});
            node.error(e);
        });

        // cleanup on flow stop
        node.on("close", function (done) {
            engine.close(node);
            done();
        });
    }
    RED.nodes.registerType("thingml-preparedeploy-docker", ThingMLPrepareDeployDockerNode);
}
